package atw.androidkeys;

import android.graphics.Paint;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.media.AudioManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputConnection;

/**
 * Created by ATW on 7/7/16.
 * Class for custom keyboard
 */
public class DakotahIME extends InputMethodService
        implements KeyboardView.OnKeyboardActionListener {

    private KeyboardView kv;

    private Keyboard keyboard;

    private boolean caps = false;
    private boolean accent = false;

    @Override
    public View onCreateInputView() {
        kv = (KeyboardView)getLayoutInflater().inflate(R.layout.keyboard, null);
        keyboard = new Keyboard(this, R.xml.qwerty);
        kv.setKeyboard(keyboard);
        kv.setOnKeyboardActionListener(this);
        return kv;
    }


    @Override
    public void onKey(int primaryCode, int[] keyCodes) {
        InputConnection ic = getCurrentInputConnection();
        playClick(primaryCode);
        switch(primaryCode){

            case Keyboard.KEYCODE_DELETE :
                CharSequence text = ic.getTextBeforeCursor(2, 0);
                // ensures that the delete function will delete both the accent and the p
                if (text.equals("p\u0323")|| text.equals("P\u0323")) {
                    ic.deleteSurroundingText(2, 0);
                } else {
                    ic.deleteSurroundingText(1, 0);
                }
                break;
            case Keyboard.KEYCODE_SHIFT:
                caps = !caps;
                keyboard.setShifted(caps);
                kv.invalidateAllKeys();
                break;
            case Keyboard.KEYCODE_DONE:
                ic.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER));
                break;
            default:
                char code = (char)primaryCode;
                if(Character.isLetter(code) && caps){
                    code = Character.toUpperCase(code);
                }
                ic.commitText(String.valueOf(code),1);
        }
    }

    @Override
    public void onPress(int primaryCode) {
    }

    @Override
    public void onRelease(int primaryCode) {
    }

    @Override
    public void onText(CharSequence text) {
        // switches between keyboards
        if (text.equals("accent")) {
            changeKeyboards(text);
        }
        else if (text.equals("numeric")) {
            changeKeyboards(text);
        }
        else if (text.equals("second")) {
            changeKeyboards(text);
        }
        else {
            // types the accented p
            if (caps) {
                getCurrentInputConnection().commitText("P̣", 1);
            } else {
                getCurrentInputConnection().commitText(text, 1);
            }
        }
        playClick(113);
    }

    @Override
    public void swipeDown() {
    }

    @Override
    public void swipeLeft() {
    }

    @Override
    public void swipeRight() {
    }

    @Override
    public void swipeUp() {
    }

    private void playClick(int keyCode){
        AudioManager am = (AudioManager)getSystemService(AUDIO_SERVICE);
        switch(keyCode){
            case 32:
                am.playSoundEffect(AudioManager.FX_KEYPRESS_SPACEBAR);
                break;
            case Keyboard.KEYCODE_DONE:
            case 10:
                am.playSoundEffect(AudioManager.FX_KEYPRESS_RETURN);
                break;
            case Keyboard.KEYCODE_DELETE:
                am.playSoundEffect(AudioManager.FX_KEYPRESS_DELETE);
                break;
            default: am.playSoundEffect(AudioManager.FX_KEYPRESS_STANDARD);
        }
    }

    public View changeKeyboards(CharSequence text) {
        // switches between keyboards
        if (text.equals("accent")) {
            if (accent) {
                keyboard = new Keyboard(this, R.xml.qwerty);
                kv.setKeyboard(keyboard);
                kv.setOnKeyboardActionListener(this);
                accent = !accent;
                if (caps) {
                    keyboard.setShifted(caps);
                    kv.invalidateAllKeys();
                }

                return kv;
            } else {
                keyboard = new Keyboard(this, R.xml.qwertyaccented);
                kv.setKeyboard(keyboard);
                kv.setOnKeyboardActionListener(this);
                accent = !accent;
                if (caps) {
                    keyboard.setShifted(caps);
                    kv.invalidateAllKeys();
                }

                return kv;
            }
        }
        else if (text.equals("numeric")) {
            caps = false;
            accent = true;
            keyboard = new Keyboard(this, R.xml.numeric);
            kv.setKeyboard(keyboard);
            kv.setOnKeyboardActionListener(this);
            return kv;
        }
        else {
            caps = false;
            accent = true;
            keyboard = new Keyboard(this, R.xml.numericsecond);
            kv.setKeyboard(keyboard);
            kv.setOnKeyboardActionListener(this);
            return kv;
        }
    }
}