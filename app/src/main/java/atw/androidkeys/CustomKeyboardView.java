package atw.androidkeys;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.util.AttributeSet;

import java.util.List;

/**
 * Created by livphillips on 7/12/16.
 * Class to modify the onDraw() function of KeyboardView, in order to get rid of bold.
 */
public class CustomKeyboardView extends KeyboardView {
    public CustomKeyboardView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onDraw(Canvas canvas){

        Paint mpaint = new Paint();
        mpaint.setTypeface(Typeface.DEFAULT);
        mpaint.setTextSize(50);
        mpaint.setColor(-1);
        System.out.println("bounds - " + canvas.getClipBounds());


        List<Keyboard.Key> keys = getKeyboard().getKeys();
        for (Keyboard.Key key : keys) {

            if (key.label != null) {
                String keyLabel = key.label.toString();
                canvas.drawText(keyLabel, key.x + key.width, key.y + key.height, mpaint);
            } else if (key.icon != null) {
                key.icon.setBounds(key.x, key.y, key.x + key.width, key.y + key.height);
                key.icon.draw(canvas); }
        }
    }


}
